import java.util.PriorityQueue;

class Solution {
    public int furthestBuilding(int[] heights, int bricks, int ladders) {
        PriorityQueue<Integer> ladderGaps = new PriorityQueue<>();

        for (int i = 0; i < heights.length - 1; i++) {
            int diff = heights[i + 1] - heights[i];
            if (diff > 0) {
                if (ladders > 0) {
                    ladderGaps.offer(diff);
                    if (ladderGaps.size() > ladders) {
                        bricks -= ladderGaps.poll();
                        if (bricks < 0) {
                            return i;
                        }
                    }
                } else {
                    bricks -= diff;
                    if (bricks < 0) {
                        return i;
                    }
                }
            }
        }
        return heights.length - 1;
    }
}

public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        
        // Example usage:
        int[] heights1 = {4, 2, 7, 6, 9, 14, 12};
        int bricks1 = 5;
        int ladders1 = 1;
        System.out.println(solution.furthestBuilding(heights1, bricks1, ladders1)); // Output: 4
        
        int[] heights2 = {4, 12, 2, 7, 3, 18, 20, 3, 19};
        int bricks2 = 10;
        int ladders2 = 2;
        System.out.println(solution.furthestBuilding(heights2, bricks2, ladders2)); // Output: 7
        
        int[] heights3 = {14, 3, 19, 3};
        int bricks3 = 17;
        int ladders3 = 0;
        System.out.println(solution.furthestBuilding(heights3, bricks3, ladders3)); // Output: 3
    }
}
